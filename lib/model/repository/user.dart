import '../entity/user.dart';

class UserRepository {
  final _users = <String, UserEntity>{};

  UserRepository() {
    _users["andrespaez@email.com"] = UserEntity(
        email: "andrespaez@email.com",
        name: "Andres Fernando Paez Suarez",
        address: "calle falsa 1234",
        phone: "3175756880",
        isAdmin: true);
    _users["camilo@email.com"] = UserEntity(
        email: "camilo@email.com",
        name: "Camilo Suarez Paez",
        address: "calle verdad 5678",
        phone: "3103146145",
        isAdmin: false);
  }

  UserEntity findByEmail(String email) {
    var user = _users[email];
    if (user == null) {
      throw Exception("Usuario no existe");
    }

    return user;
  }

  void save(UserEntity user) {
    print(user);
  }
}
