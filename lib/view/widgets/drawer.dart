import 'package:flutter/material.dart';

import '../pages/profile.dart';
import '../pages/settings.dart';

class DrawerWidget extends StatelessWidget {
  final String email;
  final String name;
  const DrawerWidget({super.key, required this.email, required this.name});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 240, 153, 23),
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.message),
            title: const Text('Mensajes'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Ajustes de Perfil'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => SettingsProfile(
                    email: email,
                    name: name,
                  ),
                ),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.construction),
            title: const Text('Configuración'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => const Settings(),
                ),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Cerrar Sesión'),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _header() {
    //todo: consutlar los datos de las cabeceras
    const image = Icon(Icons.manage_accounts);

    return SafeArea(
      child: Center(
        child: SingleChildScrollView(
          child: Row(
            children: [
              const CircleAvatar(
                child: image,
              ),
              const SizedBox(
                width: 16,
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      name,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      email,
                      style: const TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
