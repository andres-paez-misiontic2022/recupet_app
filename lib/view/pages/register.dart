import 'package:flutter/material.dart';
import '../../controller/request/register.dart';
import '../../controller/login.dart';

/*class RegisterRequest {
  late String name;
  late String address;
  late String email;
  late String phone;
  late String password;
  //late String confirmpassword;

  @override
  String toString() {
    return "$name, $address, $email, $phone, $password, ";
  }
}*/

class RegisterPage extends StatelessWidget {
  final _imageUrl = "assets/icons/Recupet logo fondo blanco.png";
  //bool light0 = false;

  late RegisterRequest _data;
  late LoginController _controller;

  RegisterPage({super.key}) {
    _data = RegisterRequest();
    _controller = LoginController();
  }
  // final MaterialStateProperty<Icon?> thumbIcon =
  //     MaterialStateProperty.resolveWith<Icon?>(
  //   (Set<MaterialState> states) {
  //     // Thumb icon when the switch is selected.
  //     if (states.contains(MaterialState.selected)) {
  //       return const Icon(Icons.check);
  //     }
  //     return const Icon(Icons.close);
  //   },
  // );

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Registro"),
        backgroundColor: const Color.fromARGB(255, 20, 95, 23),
      ),
      backgroundColor: const Color.fromARGB(255, 149, 226, 188),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Image.asset(
                  _imageUrl,
                  scale: 2,
                ),
                const SizedBox(height: 5), //espaciado de renglón
                const Text(
                  "Recupet",
                  style: TextStyle(
                    color: Color.fromARGB(255, 20, 95, 23),
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
                const Text(
                  "Primera vez? Registra tus datos!",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                // Switch(
                //   value: light0,
                //   onChanged: (bool value) {
                //     // setState(() {
                //     //   light0 = value;
                //     // });
                //   },
                // ),

                const SizedBox(height: 10), //espaciado de renglón
                _basicWidget(
                  "Nombre",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.name = newValue!;
                  },
                ),

                _basicWidget(
                  "Dirección",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.address = newValue!;
                  },
                ),
                _basicWidget(
                  "Correo Electrónico",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.email = newValue!;
                  },
                ),
                _basicWidget(
                  "Teléfono ",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.phone = newValue!;
                  },
                ),
                _basicWidget(
                  "Contraseña",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.password = newValue!;
                  },
                  isPassword: true,
                ),

                /*_basicWidget(
                  "Confirma tu Contraseña",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.confirmpassword = newValue!;
                  },
                ),*/

                ElevatedButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      formKey.currentState!.save();
                      try {
                        await _controller.registerNewUser(_data);
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content:
                                  Text("Usuario registrado exitosamente!")),
                        );
                        Navigator.pop(context);
                      } catch (error) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(error.toString()),
                          ),
                        );
                      }
                    }
                  },
                  child: const Text("Registrar"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? validarCampoObligatorio(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  Widget _basicWidget(String title, FormFieldValidator<String?> validate,
      FormFieldSetter<String?> save,
      {bool isPassword = false}) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
        child: TextFormField(
          //maxLength: 50,
          obscureText: isPassword,
          //initialValue: "N/A",
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            labelText: title,
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(12),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
              borderRadius: BorderRadius.circular(12),
            ),
            //hintText: "Ingresa tu Nombre",
            fillColor: Colors.grey[200],
            filled: true,
          ),
          validator: validate,
          onSaved: save,
        ),
      ),
    );
  }

/*
Widget _basicWidget(String title, FormFieldValidator<String?> validate,
      FormFieldSetter<String?> save,
      {bool isPassword = false}) {
        final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: TextFormField(
          //maxLength: 50,
          obscureText: isPassword,
          //initialValue: "N/A",
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            labelText: title,
          ),
          validator: validate,
          onSaved: save,
        ),
      ),
    );
  }
*/

}
