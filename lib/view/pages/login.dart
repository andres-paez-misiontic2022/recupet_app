import 'package:flutter/material.dart';
import '../../controller/login.dart';
import '../../controller/request/login.dart';
import 'home.dart';
import 'forgot_pw.dart';

import 'register.dart';

class LoginPage extends StatelessWidget {
  final _imageUrl = "assets/icons/Recupet logo fondo verde.png";
  late LoginController _controller;
  late LoginRequest _request;

  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 149, 226, 188),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _logo(),
                  _formulario(context),
                  _olvidasteContrasena(context),
                  _inicioAlternativo(),
                  _noEstasRegistrado(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(height: 5), //espaciado de renglón
// agrega el logo de recupet
        Container(
          padding: const EdgeInsets.all(10.0),
          child: Image.asset(
            _imageUrl,
            scale: 1,
          ),
        ),
        const SizedBox(height: 5), //espaciado de renglón
// saludo superior
        const Text(
          "Bienvenido a Recupet",
          style: TextStyle(
            color: Color.fromARGB(255, 20, 95, 23),
            fontWeight: FontWeight.bold,
            fontSize: 30,
          ),
        ),
        const SizedBox(height: 20), //espaciado de renglón
      ],
    );
  }

  Widget _formulario(context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoEmail(),
          const SizedBox(height: 15), //espaciado de renglón
// constraseña textfield
          _campoPassword(),
          const SizedBox(height: 10), //espaciado de renglón
// boton de ingreso
          ElevatedButton(
            child: const Text(
              "Iniciar sesión",
              style: TextStyle(fontSize: 18),
            ),
//en el siguiente se coloca un signo ! solo ejecute el metodo
//si esto es un valor diferente de nulo de lo contrario devuelve false
            onPressed: () async {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                //validar correo y contraseña en la base de datos
                try {
                  var name = await _controller.validateEmailPassword(_request);

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(
                        email: _request.email,
                        name: name,
                      ),
                    ),
                  );
                } catch (e) {
                  //showDialog(
                  // context: context,
                  // builder: (context) => AlertDialog(
                  //       title: const Text("Notificación: Recupet"),
                  //       content: Text(e.toString()),
                  //     ));
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(e.toString())));
                }
              }
            }, //se configura la sección de ingreso
          ),
          const SizedBox(height: 16), //espaciado de renglón]
        ],
      ),
    );
  }

  Widget _campoEmail() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        maxLength: 100,
        keyboardType: TextInputType.emailAddress,

        //controller: _emailController,
        decoration: InputDecoration(
          label: const Text("Ingresa tu correo electrónico"),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Color.fromARGB(255, 20, 95, 23),
            ),
            borderRadius: BorderRadius.circular(12),
          ),
          //hintText: "Ingresa tu correo electrónico",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El correo electrónico es obligatorio";
          }
          if (!value.contains("@") || !value.contains(".")) {
            return "El correo tiene un formato inválido.";
          }
          return null;
        },
        onSaved: (value) {
          _request.email = value!;
        },
      ),
    );
  }

  Widget _campoPassword() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        maxLength: 30,
        obscureText: true,
        //controller: _passwordController,
        decoration: InputDecoration(
          label: const Text("Ingresa tu contraseña"),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          //hintText: "Ingresa tu contraseña",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "La contraseña es obligatoria";
          }
          if (value.length < 6) {
            return "Mínimo debe contener 6 caracteres";
          }
          return null;
        },
        onSaved: (value) {
          _request.password = value!;
        },
      ),
    );
  }

  /*Widget _olvidoContrasena(context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return const ForgotPasswordPage();
                      },
                    ),
                  );
                },
                child: const Text(
                  "¿Olvidaste tu contraseña?",
                  style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }*/

  Widget _inicioAlternativo() {
    return Column(
      children: [
// ingreso alternativo
        const SizedBox(height: 10), //espaciado de renglón
        const Text(
          "También puedes inciciar sesión con: ",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 16), //espaciado de renglón
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () {},
              child: const Text("Facebook"),
            ),
            ElevatedButton(
              onPressed: () {},
              child: const Text("Google"),
            ),
          ],
        ),
        const SizedBox(height: 16), //espaciado de renglón
      ],
    );
  }

  /*Widget _noRegistrado(context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              "No estas registrado?",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const NewUser();
                    },
                  ),
                );
              },
              child: const Text(
                " Registrate ahora!",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        )
      ],
    );
  }*/

  Widget _olvidasteContrasena(BuildContext context) {
    return // Olvidaste tu contraseña?
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return const ForgotPasswordPage();
                  },
                ),
              );
            },
            child: const Text(
              "¿Olvidaste tu contraseña?",
              style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _noEstasRegistrado(BuildContext context) {
    return // no estas registrado? registrate ahora
        //_noRegistrado(context),
        Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "No estas registrado?",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RegisterPage()),
            );
          },
          child: const Text(
            " Registrate ahora!",
            style: TextStyle(
              color: Colors.blue,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
