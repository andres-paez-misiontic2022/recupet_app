import 'package:flutter/material.dart';
import '/view/widgets/drawer.dart';
import 'form_reporte.dart';

class HomePage extends StatelessWidget {
  final String email;
  final String name;
  final _imageUrl = "assets/icons/Recupet logo fondo blanco.png";
  const HomePage({super.key, required this.email, required this.name});

//////////////////

//////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 20, 95, 23),
        title: const Text("Recupet"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: IconButton(
              icon: const Icon(Icons.add_alert),
              //tooltip: 'Show Snackbar',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const FormReport(),
                  ),
                );
              },
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset(
              _imageUrl,
              scale: 1,
            ),
          ),
        ],
      ),
      drawer: DrawerWidget(email: email, name: name),
      body: Column(children: const [
        Text("Hola"),
      ]),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            label: 'Inicio',
            backgroundColor: Color.fromARGB(255, 20, 95, 23),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.hotel_sharp,
              color: Colors.white,
            ),
            label: 'Hogares y Vets',
            backgroundColor: Color.fromARGB(255, 20, 95, 23),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shop,
              color: Colors.white,
            ),
            label: 'MarketPlace',
            backgroundColor: Color.fromARGB(255, 20, 95, 23),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.campaign,
              color: Colors.white,
            ),
            label: 'Notificaciones',
            backgroundColor: Color.fromARGB(255, 20, 95, 23),
          ),
        ],
        //currentIndex: _selectedIndex,
        //selectedItemColor: Colors.amber[800],
        //onTap: _onItemTapped,
      ),
    );
  }
}
