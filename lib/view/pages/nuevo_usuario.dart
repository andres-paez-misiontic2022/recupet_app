/*import 'package:flutter/material.dart';

import 'login.dart';

class NewUser extends StatelessWidget {
  final _imageUrl = "assets/icons/Recupet logo fondo blanco.png";
  const NewUser({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 149, 226, 188),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 30), //espaciado de renglón
              Image.asset(
                _imageUrl,
                scale: 2,
              ),
              const SizedBox(height: 5), //espaciado de renglón
              const Text(
                "Recupet",
                style: TextStyle(
                  color: Color.fromARGB(255, 20, 95, 23),
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
              ),
              const Text(
                "Primera vez? Registra tus datos!",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 10), //espaciado de renglón

              _formulario(),

              const SizedBox(height: 10), //espaciado de renglón
              const Text("o también puedes registrarte con: "),
              const SizedBox(height: 10), //espaciado de renglón

              // tambien puedes registrarte con facebook y google
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text("Facebook"),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text("Google"),
                  ),
                ],
              ),

              const SizedBox(height: 10), //espaciado de renglón
              // no estas registrado? registrate ahora
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Ya soy parte de Recupet",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                      );
                    },
                    child: const Text(
                      " Ingresa ahora!",
                      style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario() {
    final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Column(children: [
        _campoNombre(),
        const SizedBox(height: 10), //espaciado de renglón
        const Text(
            "si quiero ser hogar de paso, habilito el campo de dirección"),
        const SizedBox(height: 10), //espaciado de renglón
        _campoDireccion(),
        const SizedBox(height: 10), //espaciado de renglón
        //_edad(),
        //const SizedBox(height: 10), //espaciado de renglón
        _campoCorreoElectronico(), // todo: validador si quiere ser hogar de paso
        const SizedBox(height: 10), //espaciado de renglón
        _campoContrasena(),
        const SizedBox(height: 10), //espaciado de renglón
        _campoConfirmaContrasena(),
        const SizedBox(height: 10), //espaciado de renglón

//botón registra usuario nuevo
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25.0),
          child: GestureDetector(
            //onTap: signUp,
            child: Container(
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 20, 95, 23),
                borderRadius: BorderRadius.circular(12),
              ),
              child: const Center(
                child: Text(
                  "Registrate ahora!",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }

  Widget _campoNombre() {
    return
        //ingresa tu nombre
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _firstNameController,
        autofocus: false,
        keyboardType: TextInputType.name,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Ingresa tu Nombre",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: ((value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        }),
      ),
    );
  }

  Widget _campoDireccion() {
    return //ingresa tu apellido
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _lastNameController,
        keyboardType: TextInputType.streetAddress,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Ingresa tu Dirección",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: ((value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        }),
      ),
    );
  }

  /*Widget _edad() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextField(
        //controller: _ageController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Ingresa tu Edad",
          fillColor: Colors.grey[200],
          filled: true,
        ),
      ),
    );
  }*/

  Widget _campoCorreoElectronico() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
          //maxLength: 100,
          keyboardType: TextInputType.emailAddress,
          //controller: _emailController,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(12),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Color.fromARGB(255, 20, 95, 23),
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            hintText: "Ingresa tu correo electrónico",
            fillColor: Colors.grey[200],
            filled: true,
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "El correo electrónico es obligatorio.";
            }
            if (!value.contains("@") || value.contains(".")) {
              return "El correo tiene un formato inválido.";
            }
            return null;
          }),
    );
  }

  Widget _campoContrasena() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
          maxLength: 30,
          obscureText: true,
          //controller: _passwordController,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(12),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
              borderRadius: BorderRadius.circular(12),
            ),
            hintText: "Ingresa tu contraseña",
            fillColor: Colors.grey[200],
            filled: true,
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "La contraseña es obligatoria.";
            }
            if (value.length < 6) {
              return "Mínimo debe contener 6 caracteres.";
            }
            return null;
          }),
    );
  }

  Widget _campoConfirmaContrasena() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
          maxLength: 30,
          obscureText: true,
          //controller: _passwordController,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(12),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
              borderRadius: BorderRadius.circular(12),
            ),
            hintText: "Confirmar la Contraseña",
            fillColor: Colors.grey[200],
            filled: true,
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return "La contraseña es obligatoria.";
            }
            if (value.length < 6) {
              return "Mínimo debe contener 6 caracteres.";
            }
            return null;
          }),
    );
  }
}*/
