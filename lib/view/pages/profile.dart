import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class SettingsProfile extends StatelessWidget {
  const SettingsProfile(
      {super.key, required String email, required String name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 240, 153, 23),
        title: const Text("Ajustes de Perfil"),
      ),
      drawer: const DrawerWidget(email: "", name: ""),
    );
  }
}
