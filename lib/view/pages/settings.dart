import 'package:flutter/material.dart';
import '../widgets/drawer.dart';

class Settings extends StatelessWidget {
  const Settings({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 240, 153, 23),
        title: const Text("Configuraciones"),
      ),
      drawer: const DrawerWidget(email: "", name: ""),
    );
  }
}
