import 'package:flutter/material.dart';

class FormReport extends StatelessWidget {
  final _imageUrl = "assets/icons/Recupet logo fondo blanco.png";
  const FormReport({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 20, 95, 23),
        title: const Text("Formulario de Reporte"),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              //const SizedBox(height: 1), //espaciado de renglón
              Image.asset(
                _imageUrl,
                scale: 3,
              ),
              const SizedBox(height: 5), //espaciado de renglón
              const Text(
                "Recupet",
                style: TextStyle(
                  color: Color.fromARGB(255, 20, 95, 23),
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              const Text(
                "Avisa a la comunidad!",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 10), //espaciado de renglón
              _formulario(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context) {
    //final formKey = GlobalKey<FormState>();

    return Form(
      child: Column(
        children: [
          _campoNombreQuienReporta(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoTelefonoContacto(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoDireccion(),
          const SizedBox(height: 10), //espaciado de renglón
          _tipoReporte(),
          const SizedBox(height: 10), //espaciado de renglón

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _campoFecha(),
              //_campoFecha(),
              const SizedBox(width: 10), //espaciado de renglón
              //hora
              _campoHora(),
              //_campoHora(),
            ],
          ),

          const SizedBox(height: 10), //espaciado de renglón
          _campoFotos(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoNombreMascota(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoRazaMascota(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoColorMascota(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoGeneroMascota(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoEdadMascota(),
          const SizedBox(height: 10), //espaciado de renglón
          _campoInformacionAdicional(),
          const SizedBox(height: 10), //espaciado de renglón

          //boton de publicar
          /*ElevatedButton(
            child: const Text("Publicar Reporte"),
            onPressed: () {
              if (formKey.currentState!.validate()) {
                //todo: guardar los datos en la BD
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text("Reporte Publicado con exito"),
                  ),
                );
              }
            },
          ),*/

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: GestureDetector(
              child: Container(
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 20, 95, 23),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: const Center(
                  child: Text(
                    "Publicar Reporte",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              //onTap: signUp,
              onTap: () {
                /*if (formKey.currentState!.validate()) {
                  //todo: guardar los datos en la bd
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Reporte Publicado con exito"),
                    ),
                  );
                }*/

                //volver a la pantalla anterior
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _campoNombreQuienReporta() {
    return //nombre de quien reporta

        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        autofocus: true,
        keyboardType: TextInputType.name,
        //controller: _firstNameController,
        decoration: InputDecoration(
          icon: const Icon(Icons.perm_identity),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Nombre de quien reporta",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }

  Widget _campoTelefonoContacto() {
    return //Telefono de contacto
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.phone,
        //controller: _lastNameController,
        decoration: InputDecoration(
          icon: const Icon(Icons.phone),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Telefono de contacto",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          if (int.parse(value) < 1) {
            return "El valor no es valido";
          }
          return null;
        },
      ),
    );
  }

  Widget _campoDireccion() {
    return //direccion
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _lastNameController,
        keyboardType: TextInputType.streetAddress,
        decoration: InputDecoration(
          icon: const Icon(Icons.location_on_outlined),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Ingresa tu Dirección",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }

  Widget _tipoReporte() {
    var opciones = <String>[
      "Desaparecido",
      "Encontrado",
      "Maltratado",
      "Abandonado"
    ];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: DropdownButtonFormField(
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
        decoration: InputDecoration(
          icon: const Icon(Icons.assignment_turned_in_sharp),
          labelText: "Tipo de Reporte",
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          //hintText: "Género de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        textAlign: TextAlign.center,
                        value,
                      ),
                    ))
            .toList(),
        /*items: const [
          DropdownMenuItem(value: "Desaparecido", child: Text("Desaparecido")),
          DropdownMenuItem(value: "Encontrado", child: Text("Encontrado")),
          DropdownMenuItem(value: "Maltratado", child: Text("Maltratado")),
          DropdownMenuItem(value: "Abandonado", child: Text("Abandonado")),
        ],*/
        /*value: _dropdownValue,
                  onChanged: (dropdownCallback) {},*/
        //value: _dropdownValue,
        onChanged: (value) {},
        iconSize: 30.0,
        iconEnabledColor: const Color.fromARGB(255, 20, 95, 23),
        //iconEnabledColor: Colors.grey[200],
        isExpanded: false,
        //icon: const Icon(Icons.flutter_dash),
      ),
    );
  }

  Widget _campoFecha() {
    return Column(
      children: const [
        Text("fecha"),
      ],
    );
  }

  Widget _campoHora() {
    return Column(
      children: const [
        Text("hora"),
      ],
    );
  }

  Widget _campoFotos() {
    return Column(
      children: const [
        Text("Adjuntar Foto"),
      ],
    );
  }

  Widget _campoNombreMascota() {
    return
        //Nombre de la mascota (opcional)
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _ageController,
        keyboardType: TextInputType.name,
        decoration: InputDecoration(
          icon: const Icon(Icons.pets),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Nombre de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }

  Widget _campoRazaMascota() {
    return //Raza de la mascota (opcional)
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _emailController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Raza de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }

  Widget _campoColorMascota() {
    return
        //Color de la mascota
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _emailController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Color de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }

  /*Widget _campoGeneroMascota() {
    return
        //Género de la mascota
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _emailController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Género de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        /*validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },*/
      ),
    );
  }*/

  Widget _campoGeneroMascota() {
    var opciones = <String>["Macho", "Hembra"];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: DropdownButtonFormField(
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
        decoration: InputDecoration(
          //icon: const Icon(Icons.assignment_turned_in_sharp),
          labelText: "Género de la mascota",
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          //hintText: "Género de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        textAlign: TextAlign.center,
                        value,
                      ),
                    ))
            .toList(),
        /*items: const [
          DropdownMenuItem(value: "Desaparecido", child: Text("Desaparecido")),
          DropdownMenuItem(value: "Encontrado", child: Text("Encontrado")),
          DropdownMenuItem(value: "Maltratado", child: Text("Maltratado")),
          DropdownMenuItem(value: "Abandonado", child: Text("Abandonado")),
        ],*/
        /*value: _dropdownValue,
                  onChanged: (dropdownCallback) {},*/
        //value: _dropdownValue,
        onChanged: (value) {},
        iconSize: 30.0,
        iconEnabledColor: const Color.fromARGB(255, 20, 95, 23),
        //iconEnabledColor: Colors.grey[200],
        isExpanded: false,
        //icon: const Icon(Icons.flutter_dash),
      ),
    );
  }

  Widget _campoEdadMascota() {
    return
        //Edad de la mascota
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        //controller: _emailController,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Edad de la mascota",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          if (int.parse(value) < 1) {
            return "El valor no es valido";
          }
          return null;
        },
      ),
    );
  }

  Widget _campoInformacionAdicional() {
    return
        //Información adicional
        Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: TextFormField(
        keyboardType: TextInputType.text,
        obscureText: true,
        //controller: _confirmpasswordController,
        decoration: InputDecoration(
          icon: const Icon(Icons.textsms),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(12),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: Color.fromARGB(255, 20, 95, 23)),
            borderRadius: BorderRadius.circular(12),
          ),
          hintText: "Información adicional",
          fillColor: Colors.grey[200],
          filled: true,
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo nombre es obligatorio";
          }
          return null;
        },
      ),
    );
  }
}
