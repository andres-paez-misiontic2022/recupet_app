import '../model/entity/user.dart';
import '../model/repository/fb_auth.dart';
import '../model/repository/user.dart';
import 'request/login.dart';
import 'request/register.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
  }

  Future<String> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);
    // consultar el usuario que tenga el correo dado
    var user = _userRepository.findByEmail(request.email);

    return user.name!;
  }

  Future<void> registerNewUser(RegisterRequest request,
      {bool adminUser = false}) async {
// Crear un correo clave en firebase authentication
    var value = await _authRepository.createEmailPasswordAccount(
        request.email, request.password);

// agregar informacion adicional en la base de datos
    _userRepository.save(UserEntity(
        name: request.name,
        address: request.address,
        email: request.email,
        phone: request.phone,
        isAdmin: adminUser));
  }
}
